color_text = "#212121"
color_background = "#ffffff"
color_background_header = "#1976d2"

repos = [
    {
        "repo": "https://f-droid.org/repo",
        "site": "https://f-droid.org",
        "icon-mirror": "https://ftp.fau.de/fdroid/repo",
    },
]

langs = [
    "ar",
    "bo",
    "en",
    "de",
    "es",
    "fa",
    "fr",
    "he",
    "hu",
    "id",
    "is",
    "it",
    "ja",
    "ko",
    "pl",
    "pt",
    "pt_BR",
    "ro",
    "ru",
    "sq",
    "tr",
    "uk",
    "zh_Hans",
    "zh_Hant",
]
